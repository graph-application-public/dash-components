let MockStructure = [
  {
    "r": {
      "identity": 1238,
      "start": 1005,
      "end": 1006,
      "type": "HAS",
      "properties": {
        "default": true
      }
    },
    "n1": {
      "identity": 1005,
      "labels": [
        "API"
      ],
      "properties": {
        "authorization": "Bearer yqMKA3TBxpjLKmVnEnLcBg2vZKNb5r6b",
        "id": "53fccd70-5258-4f44-a43f-8cd1e19df16e",
        "type": "Apollo",
        "version": "0.0.1",
        "relaodAddress": "http://apollo:4000/api/die"
      }
    },
    "n2": {
      "identity": 1006,
      "labels": [
        "Source"
      ],
      "properties": {
        "password": "letmein",
        "id": "9c19ba34-d655-454a-b026-11baefc58e16",
        "type": "neo4j",
        "user": "neo4j",
        "url": "bolt://localhost:8687"
      }
    }
  },
  {
    "r": {
      "identity": 1239,
      "start": 1007,
      "end": 1008,
      "type": "HAS_OUTPUT",
      "properties": {}
    },
    "n1": {
      "identity": 1007,
      "labels": [
        "SchemaType"
      ],
      "properties": {
        "name": "getAppConfiguration",
        "type": "Query",
        "id": "6e9646da-e2be-4102-9221-487cbb3bbac5"
      }
    },
    "n2": {
      "identity": 1008,
      "labels": [
        "SchemaType"
      ],
      "properties": {
        "name": "view",
        "type": "String",
        "id": "0e7376c3-cea5-4a17-9df8-51d69c57a6f3"
      }
    }
  },
  {
    "r": {
      "identity": 1240,
      "start": 1007,
      "end": 1005,
      "type": "PART_OF",
      "properties": {}
    },
    "n1": {
      "identity": 1007,
      "labels": [
        "SchemaType"
      ],
      "properties": {
        "name": "getAppConfiguration",
        "type": "Query",
        "id": "6e9646da-e2be-4102-9221-487cbb3bbac5"
      }
    },
    "n2": {
      "identity": 1005,
      "labels": [
        "API"
      ],
      "properties": {
        "authorization": "Bearer yqMKA3TBxpjLKmVnEnLcBg2vZKNb5r6b",
        "id": "53fccd70-5258-4f44-a43f-8cd1e19df16e",
        "type": "Apollo",
        "version": "0.0.1",
        "relaodAddress": "http://apollo:4000/api/die"
      }
    }
  },
  {
    "r": {
      "identity": 1241,
      "start": 1007,
      "end": 1009,
      "type": "RUNS",
      "properties": {}
    },
    "n1": {
      "identity": 1007,
      "labels": [
        "SchemaType"
      ],
      "properties": {
        "name": "getAppConfiguration",
        "type": "Query",
        "id": "6e9646da-e2be-4102-9221-487cbb3bbac5"
      }
    },
    "n2": {
      "identity": 1009,
      "labels": [
        "CypherQuery"
      ],
      "properties": {
        "name": "getAppStructure",
        "value": "CALL custom.getAppStructure() YIELD structure RETURN apoc.convert.toJson(structure) as view",
        "id": "a0f7e004-4646-4bac-917b-44a8d6a07002"
      }
    }
  },
  {
    "r": {
      "identity": 1242,
      "start": 1009,
      "end": 1006,
      "type": "ON",
      "properties": {}
    },
    "n1": {
      "identity": 1009,
      "labels": [
        "CypherQuery"
      ],
      "properties": {
        "name": "getAppStructure",
        "value": "CALL custom.getAppStructure() YIELD structure RETURN apoc.convert.toJson(structure) as view",
        "id": "a0f7e004-4646-4bac-917b-44a8d6a07002"
      }
    },
    "n2": {
      "identity": 1006,
      "labels": [
        "Source"
      ],
      "properties": {
        "password": "letmein",
        "id": "9c19ba34-d655-454a-b026-11baefc58e16",
        "type": "neo4j",
        "user": "neo4j",
        "url": "bolt://localhost:8687"
      }
    }
  },
  {
    "r": {
      "identity": 1250,
      "start": 1010,
      "end": 1017,
      "type": "HAS_OUTPUT",
      "properties": {}
    },
    "n1": {
      "identity": 1010,
      "labels": [
        "SchemaType"
      ],
      "properties": {
        "name": "inputNpsForm",
        "type": "Mutation",
        "id": "c6391378-36b0-496d-af33-70bccc2eee6c"
      }
    },
    "n2": {
      "identity": 1017,
      "labels": [
        "SchemaType"
      ],
      "properties": {
        "name": "updateSuccess",
        "type": "Boolean",
        "id": "3da75f7e-bd89-4ae0-ad23-93ef8f2c2ab9"
      }
    }
  },
  {
    "r": {
      "identity": 1243,
      "start": 1010,
      "end": 1005,
      "type": "PART_OF",
      "properties": {}
    },
    "n1": {
      "identity": 1010,
      "labels": [
        "SchemaType"
      ],
      "properties": {
        "name": "inputNpsForm",
        "type": "Mutation",
        "id": "c6391378-36b0-496d-af33-70bccc2eee6c"
      }
    },
    "n2": {
      "identity": 1005,
      "labels": [
        "API"
      ],
      "properties": {
        "authorization": "Bearer yqMKA3TBxpjLKmVnEnLcBg2vZKNb5r6b",
        "id": "53fccd70-5258-4f44-a43f-8cd1e19df16e",
        "type": "Apollo",
        "version": "0.0.1",
        "relaodAddress": "http://apollo:4000/api/die"
      }
    }
  },
  {
    "r": {
      "identity": 1251,
      "start": 1010,
      "end": 1018,
      "type": "RUNS",
      "properties": {}
    },
    "n1": {
      "identity": 1010,
      "labels": [
        "SchemaType"
      ],
      "properties": {
        "name": "inputNpsForm",
        "type": "Mutation",
        "id": "c6391378-36b0-496d-af33-70bccc2eee6c"
      }
    },
    "n2": {
      "identity": 1018,
      "labels": [
        "CypherQuery"
      ],
      "properties": {
        "name": "inputNpsResponse",
        "value": "CREATE (g:NpsSurvey {nps: $nps, npsComment: $npsComment, safety: $safety, simple: $simple, alternative: $alternative, datetime: datetime()}) MERGE (o:Order {id:$orderId}) MERGE (o)-[:HAS_GRADE]->(g) RETURN true",
        "id": "dff33135-d7dc-42dc-8b1b-85f8e6911823"
      }
    }
  },
  {
    "r": {
      "identity": 1244,
      "start": 1011,
      "end": 1010,
      "type": "IS_INPUT",
      "properties": {
        "name": "orderId"
      }
    },
    "n1": {
      "identity": 1011,
      "labels": [
        "SchemaType"
      ],
      "properties": {
        "name": "orderId",
        "type": "String",
        "id": "eafd930f-6220-45ca-b160-f4c6c0404873"
      }
    },
    "n2": {
      "identity": 1010,
      "labels": [
        "SchemaType"
      ],
      "properties": {
        "name": "inputNpsForm",
        "type": "Mutation",
        "id": "c6391378-36b0-496d-af33-70bccc2eee6c"
      }
    }
  },
  {
    "r": {
      "identity": 1245,
      "start": 1012,
      "end": 1010,
      "type": "IS_INPUT",
      "properties": {
        "name": "nps"
      }
    },
    "n1": {
      "identity": 1012,
      "labels": [
        "SchemaType"
      ],
      "properties": {
        "name": "nps",
        "type": "String",
        "id": "47c21a57-fa7d-4285-920a-ce5516572dcb"
      }
    },
    "n2": {
      "identity": 1010,
      "labels": [
        "SchemaType"
      ],
      "properties": {
        "name": "inputNpsForm",
        "type": "Mutation",
        "id": "c6391378-36b0-496d-af33-70bccc2eee6c"
      }
    }
  },
  {
    "r": {
      "identity": 1246,
      "start": 1013,
      "end": 1010,
      "type": "IS_INPUT",
      "properties": {
        "name": "npsComment",
        "optional": true
      }
    },
    "n1": {
      "identity": 1013,
      "labels": [
        "SchemaType"
      ],
      "properties": {
        "name": "npsComment",
        "type": "String",
        "id": "d05338aa-dcb0-4340-a16d-aae80d534f57"
      }
    },
    "n2": {
      "identity": 1010,
      "labels": [
        "SchemaType"
      ],
      "properties": {
        "name": "inputNpsForm",
        "type": "Mutation",
        "id": "c6391378-36b0-496d-af33-70bccc2eee6c"
      }
    }
  },
  {
    "r": {
      "identity": 1247,
      "start": 1014,
      "end": 1010,
      "type": "IS_INPUT",
      "properties": {
        "name": "safety",
        "optional": true
      }
    },
    "n1": {
      "identity": 1014,
      "labels": [
        "SchemaType"
      ],
      "properties": {
        "name": "safety",
        "type": "String",
        "id": "f23a2f27-33dd-4820-ba3f-77c86894acd7"
      }
    },
    "n2": {
      "identity": 1010,
      "labels": [
        "SchemaType"
      ],
      "properties": {
        "name": "inputNpsForm",
        "type": "Mutation",
        "id": "c6391378-36b0-496d-af33-70bccc2eee6c"
      }
    }
  },
  {
    "r": {
      "identity": 1248,
      "start": 1015,
      "end": 1010,
      "type": "IS_INPUT",
      "properties": {
        "name": "simple",
        "optional": true
      }
    },
    "n1": {
      "identity": 1015,
      "labels": [
        "SchemaType"
      ],
      "properties": {
        "name": "simple",
        "type": "String",
        "id": "680e5a99-b1c8-4ffe-88e6-78280fb4d549"
      }
    },
    "n2": {
      "identity": 1010,
      "labels": [
        "SchemaType"
      ],
      "properties": {
        "name": "inputNpsForm",
        "type": "Mutation",
        "id": "c6391378-36b0-496d-af33-70bccc2eee6c"
      }
    }
  },
  {
    "r": {
      "identity": 1249,
      "start": 1016,
      "end": 1010,
      "type": "IS_INPUT",
      "properties": {
        "name": "alternative",
        "optional": true
      }
    },
    "n1": {
      "identity": 1016,
      "labels": [
        "SchemaType"
      ],
      "properties": {
        "name": "alternative",
        "type": "String",
        "id": "c9f427b7-cf2a-4881-aaf3-58bd6e3e1e8c"
      }
    },
    "n2": {
      "identity": 1010,
      "labels": [
        "SchemaType"
      ],
      "properties": {
        "name": "inputNpsForm",
        "type": "Mutation",
        "id": "c6391378-36b0-496d-af33-70bccc2eee6c"
      }
    }
  },
  {
    "r": {
      "identity": 1252,
      "start": 1018,
      "end": 1006,
      "type": "ON",
      "properties": {}
    },
    "n1": {
      "identity": 1018,
      "labels": [
        "CypherQuery"
      ],
      "properties": {
        "name": "inputNpsResponse",
        "value": "CREATE (g:NpsSurvey {nps: $nps, npsComment: $npsComment, safety: $safety, simple: $simple, alternative: $alternative, datetime: datetime()}) MERGE (o:Order {id:$orderId}) MERGE (o)-[:HAS_GRADE]->(g) RETURN true",
        "id": "dff33135-d7dc-42dc-8b1b-85f8e6911823"
      }
    },
    "n2": {
      "identity": 1006,
      "labels": [
        "Source"
      ],
      "properties": {
        "password": "letmein",
        "id": "9c19ba34-d655-454a-b026-11baefc58e16",
        "type": "neo4j",
        "user": "neo4j",
        "url": "bolt://localhost:8687"
      }
    }
  },
  {
    "r": {
      "identity": 1254,
      "start": 1019,
      "end": 1020,
      "type": "RUNS",
      "properties": {}
    },
    "n1": {
      "identity": 1019,
      "labels": [
        "SchemaType"
      ],
      "properties": {
        "name": "getGrades",
        "type": "Query",
        "id": "20f24a35-5c0a-44c9-ac55-9ff430eba66a"
      }
    },
    "n2": {
      "identity": 1020,
      "labels": [
        "CypherQuery"
      ],
      "properties": {
        "name": "gradesOutput",
        "value": "UNWIND range(0,10) AS idx RETURN {value:toString(idx), color: CASE WHEN idx < 7 THEN 'red' ELSE 'green' END}",
        "id": "18105684-9654-4776-a2cf-30387146a312"
      }
    }
  },
  {
    "r": {
      "identity": 1253,
      "start": 1019,
      "end": 1005,
      "type": "PART_OF",
      "properties": {}
    },
    "n1": {
      "identity": 1019,
      "labels": [
        "SchemaType"
      ],
      "properties": {
        "name": "getGrades",
        "type": "Query",
        "id": "20f24a35-5c0a-44c9-ac55-9ff430eba66a"
      }
    },
    "n2": {
      "identity": 1005,
      "labels": [
        "API"
      ],
      "properties": {
        "authorization": "Bearer yqMKA3TBxpjLKmVnEnLcBg2vZKNb5r6b",
        "id": "53fccd70-5258-4f44-a43f-8cd1e19df16e",
        "type": "Apollo",
        "version": "0.0.1",
        "relaodAddress": "http://apollo:4000/api/die"
      }
    }
  },
  {
    "r": {
      "identity": 1256,
      "start": 1019,
      "end": 1021,
      "type": "HAS_OUTPUT",
      "properties": {
        "multiple": true
      }
    },
    "n1": {
      "identity": 1019,
      "labels": [
        "SchemaType"
      ],
      "properties": {
        "name": "getGrades",
        "type": "Query",
        "id": "20f24a35-5c0a-44c9-ac55-9ff430eba66a"
      }
    },
    "n2": {
      "identity": 1021,
      "labels": [
        "SchemaType"
      ],
      "properties": {
        "name": "Grade",
        "type": "Grade",
        "id": "01f507ef-7b0a-4fe5-ad55-9a527da9fdc6"
      }
    }
  },
  {
    "r": {
      "identity": 1255,
      "start": 1020,
      "end": 1006,
      "type": "ON",
      "properties": {}
    },
    "n1": {
      "identity": 1020,
      "labels": [
        "CypherQuery"
      ],
      "properties": {
        "name": "gradesOutput",
        "value": "UNWIND range(0,10) AS idx RETURN {value:toString(idx), color: CASE WHEN idx < 7 THEN 'red' ELSE 'green' END}",
        "id": "18105684-9654-4776-a2cf-30387146a312"
      }
    },
    "n2": {
      "identity": 1006,
      "labels": [
        "Source"
      ],
      "properties": {
        "password": "letmein",
        "id": "9c19ba34-d655-454a-b026-11baefc58e16",
        "type": "neo4j",
        "user": "neo4j",
        "url": "bolt://localhost:8687"
      }
    }
  },
  {
    "r": {
      "identity": 1258,
      "start": 1021,
      "end": 1022,
      "type": "CONSISTS_OF",
      "properties": {
        "name": "value"
      }
    },
    "n1": {
      "identity": 1021,
      "labels": [
        "SchemaType"
      ],
      "properties": {
        "name": "Grade",
        "type": "Grade",
        "id": "01f507ef-7b0a-4fe5-ad55-9a527da9fdc6"
      }
    },
    "n2": {
      "identity": 1022,
      "labels": [
        "SchemaType"
      ],
      "properties": {
        "type": "String",
        "id": "65eee98c-3b13-4d1a-aded-50dc7591429e"
      }
    }
  },
  {
    "r": {
      "identity": 1257,
      "start": 1021,
      "end": 1005,
      "type": "PART_OF",
      "properties": {}
    },
    "n1": {
      "identity": 1021,
      "labels": [
        "SchemaType"
      ],
      "properties": {
        "name": "Grade",
        "type": "Grade",
        "id": "01f507ef-7b0a-4fe5-ad55-9a527da9fdc6"
      }
    },
    "n2": {
      "identity": 1005,
      "labels": [
        "API"
      ],
      "properties": {
        "authorization": "Bearer yqMKA3TBxpjLKmVnEnLcBg2vZKNb5r6b",
        "id": "53fccd70-5258-4f44-a43f-8cd1e19df16e",
        "type": "Apollo",
        "version": "0.0.1",
        "relaodAddress": "http://apollo:4000/api/die"
      }
    }
  },
  {
    "r": {
      "identity": 1259,
      "start": 1021,
      "end": 1023,
      "type": "CONSISTS_OF",
      "properties": {
        "name": "color"
      }
    },
    "n1": {
      "identity": 1021,
      "labels": [
        "SchemaType"
      ],
      "properties": {
        "name": "Grade",
        "type": "Grade",
        "id": "01f507ef-7b0a-4fe5-ad55-9a527da9fdc6"
      }
    },
    "n2": {
      "identity": 1023,
      "labels": [
        "SchemaType"
      ],
      "properties": {
        "type": "String",
        "id": "6789fd03-e5f8-4b10-9edf-fb4261d4f26e"
      }
    }
  },
  {
    "r": {
      "identity": 1260,
      "start": 1024,
      "end": 1019,
      "type": "INVOKES",
      "properties": {}
    },
    "n1": {
      "identity": 1024,
      "labels": [
        "Function"
      ],
      "properties": {
        "name": "getGrades",
        "input": [
          ""
        ],
        "id": "a460cb72-8149-4495-8baf-a5757f612f15",
        "type": "graphql"
      }
    },
    "n2": {
      "identity": 1019,
      "labels": [
        "SchemaType"
      ],
      "properties": {
        "name": "getGrades",
        "type": "Query",
        "id": "20f24a35-5c0a-44c9-ac55-9ff430eba66a"
      }
    }
  },
  {
    "r": {
      "identity": 1264,
      "start": 1025,
      "end": 1028,
      "type": "HAS",
      "properties": {}
    },
    "n1": {
      "identity": 1025,
      "labels": [
        "Application"
      ],
      "properties": {
        "name": "tus-forms",
        "id": "64ad8fec-4a8d-4597-91f9-197935971dbe",
        "title": "Question forms",
        "version": "0.0.1",
        "reloadAddress": "http://localhost:4001/api/reload"
      }
    },
    "n2": {
      "identity": 1028,
      "labels": [
        "Asset"
      ],
      "properties": {
        "type": "style",
        "value": "https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css",
        "id": "d4c5175c-212e-46b9-b4ad-8320a23d988d"
      }
    }
  },
  {
    "r": {
      "identity": 1307,
      "start": 1025,
      "end": 1065,
      "type": "CONTAINS",
      "properties": {
        "route": "/form1"
      }
    },
    "n1": {
      "identity": 1025,
      "labels": [
        "Application"
      ],
      "properties": {
        "name": "tus-forms",
        "id": "64ad8fec-4a8d-4597-91f9-197935971dbe",
        "title": "Question forms",
        "version": "0.0.1",
        "reloadAddress": "http://localhost:4001/api/reload"
      }
    },
    "n2": {
      "identity": 1065,
      "labels": [
        "View"
      ],
      "properties": {
        "tagName": "title",
        "innerHTML": "S kakšno verjetnostjo bi predlagali storitev Hitri Nakup prijatelju ali znancu?",
        "id": "f188dc8c-d2c6-47cf-b2ff-a059afce59e5"
      }
    }
  }
]


export default MockStructure