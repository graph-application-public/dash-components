import React from 'react';

import DashboardComponent from './DashboardComponent';
import MockStructure from '../../stories/StaticData';

export default {
  component: DashboardComponent,
  title: 'DashboardComponent',
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/,
};

export const dashboardData = {
  id: '1',
  title: 'Test Test',
  state: 'TEST STATE',
};


export const Default = () => <DashboardComponent graphData={MockStructure}/>;
export const Empty = () => <DashboardComponent />;
export const Loading = () => <DashboardComponent />;