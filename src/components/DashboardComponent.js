import React, { useEffect } from "react";
import { Graph } from "react-d3-graph";



const DashboardComponent = ({graphData}) => {
  let nodes = []
  let links = []

  graphData.forEach((relationship)=>{
    let source = relationship.n1
    let target = relationship.n2

    let sourceExists = nodes.find((needle) => needle.id === source.identity)
    let targetExists = nodes.find((needle) => needle.id === target.identity)

    if(!sourceExists){
      nodes.push({
        id: source.identity,
        label: source.properties.name,
        color: NODE_COLORS[Math.floor(Math.random() * NODE_COLORS.length)],
        size: DEFAULT_SIZE
      })
    } else {
      sourceExists.size *= GROWTH_FACTOR
    }

    if(!targetExists){
      nodes.push({
        id: target.identity,
        label: target.properties.name,
        color: NODE_COLORS[Math.floor(Math.random() * NODE_COLORS.length)],
        size: DEFAULT_SIZE
      })
    } else {
      targetExists.size *= GROWTH_FACTOR
    }

    links.push({
      source: source.identity,
      target: target.identity,
      label: relationship.r.type
    })
  })


  const data = {
      nodes: nodes,
      links: links,
  };

  console.log("final data before rendering:", data)

  // the graph configuration, you only need to pass down properties
  // that you want to override, otherwise default ones will be used
  const myConfig = {
      nodeHighlightBehavior: true,
      node: {
          color: "lightgreen",
          size: DEFAULT_SIZE,
          highlightStrokeColor: "blue",
          labelProperty: "label"
      },
      link: {
          highlightColor: "lightblue",
          renderLabel: true
      },
  };

  // graph event callbacks
  const onClickGraph = function() {
      console.log(`Clicked the graph background`);
  };

  const onClickNode = function(nodeId) {
      console.log(`Clicked node ${nodeId}`);
  };

  const onDoubleClickNode = function(nodeId) {
      console.log(`Double clicked node ${nodeId}`);
  };

  const onRightClickNode = function(event, nodeId) {
      console.log(`Right clicked node ${nodeId}`);
  };

  const onMouseOverNode = function(nodeId) {
      console.log(`Mouse over node ${nodeId}`);
  };

  const onMouseOutNode = function(nodeId) {
      console.log(`Mouse out node ${nodeId}`);
  };

  const onClickLink = function(source, target) {
      console.log(`Clicked link between ${source} and ${target}`);
  };

  const onRightClickLink = function(event, source, target) {
      console.log(`Right clicked link between ${source} and ${target}`);
  };

  const onMouseOverLink = function(source, target) {
      console.log(`Mouse over in link between ${source} and ${target}`);
  };

  const onMouseOutLink = function(source, target) {
      console.log(`Mouse out link between ${source} and ${target}`);
  };

  const onNodePositionChange = function(nodeId, x, y) {
      console.log(`Node ${nodeId} is moved to new position. New position is x= ${x} y= ${y}`);
  };


  return (
    <Graph
        id="graph-id" // id is mandatory, if no id is defined rd3g will throw an error
        data={data}
        config={myConfig}
        onClickNode={onClickNode}
        onDoubleClickNode={onDoubleClickNode}
        onRightClickNode={onRightClickNode}
        onClickGraph={onClickGraph}
        onClickLink={onClickLink}
        onRightClickLink={onRightClickLink}
        onMouseOverNode={onMouseOverNode}
        onMouseOutNode={onMouseOutNode}
        onMouseOverLink={onMouseOverLink}
        onMouseOutLink={onMouseOutLink}
        onNodePositionChange={onNodePositionChange}
    />
  );
}

export default DashboardComponent;


const NODE_COLORS = [
  "#ef476f",
  "#ffd166",
  "#06d6a0",
  "#118ab2",
  "#073b4c",
  "#9b5de5",
  "#f15bb5",
  "#00bbf9",
  "#00f5d4",

]

const DEFAULT_SIZE = 600

const GROWTH_FACTOR = 1.3